from django.apps import AppConfig


class MsprApiConfig(AppConfig):
    name = 'MSPR_API'
